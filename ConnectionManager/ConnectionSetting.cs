﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ODBCMngr;
using System.Windows.Forms;

namespace ODBCTest
{
    public partial class ConnectionSetting : Form
    {
        public ConnectionSetting()
        {
            InitializeComponent();
        }

        private void ConnectionSetting_Load(object sender, EventArgs e)
        {
            ListMessageLog.Items.Clear();
            cboConnectionType.Items.Clear();
            cboConnectionType.Items.Add("ODBC DSN");
            cboConnectionType.Items.Add("ODBC (Direct Driver)");
        }

       

        private void btnShowPassword_Click(object sender, EventArgs e)
        {
            if(btnShowPassword.Text.ToLower() == "show")
            {
                char PasswordCharacter ;
                char.TryParse("", out PasswordCharacter);
                txtPassword.PasswordChar = PasswordCharacter;
                btnShowPassword.Text = "Hide";

            }
            else
            {
                txtPassword.PasswordChar = char.Parse("*");
                btnShowPassword.Text = "Show";

            }
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            try
            { 
                ODBCData c_ODBCData = new ODBCData();
                string DSN_Name = "";
                string Server = "";
                if (lblServer.Text == "DSN")
                    DSN_Name = cboDSNList.Text;
                if (lblServer.Text == "Server")
                    Server = cboDSNList.Text;

                string TestResult = c_ODBCData.ODBC_ConnectionTest(DSN_Name, cboDriver.Text, Server, cboDatabase.Text, txtUser.Text, txtPassword.Text,txtoptions.Text);
                ListMessageLog.Items.Add(TestResult);

                
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void cboConnectionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboDSNList.Items.Clear();
            cboDriver.Items.Clear();
            switch (cboConnectionType.SelectedItem.ToString())
            {
                
                case "ODBC DSN":
                    lblServer.Text = "DSN";
                    break;
                case "ODBC (Direct Driver)":
                    lblServer.Text = "Server";
                    
                    break;
                default:
                    lblServer.Text = "Server";
                    break;
            }
        }

        private void btnBrowseDSN_Click(object sender, EventArgs e)
        {
            try {
            cboDSNList.Items.Clear();
            cboDSNList.SelectedText = "";
            cboDSNList.Text = "";
            switch (lblServer.Text)
            {
                case "DSN":
                    ODBCDSN[] DSNList = ODBCManager.GetSystemDSNList();
                    if (DSNList != null)
                    {
                        foreach (ODBCDSN dsn in DSNList)
                        {
                            cboDSNList.Items.Add(dsn.GetDSNName().ToString());

                        }
                        cboDSNList.SelectedText = cboDSNList.Items[0].ToString();
                    }
                    break;
                

            }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


        }
        private void btnBrowseDriver_Click(object sender, EventArgs e)
        {
            try
            {
                cboDriver.Items.Clear();
                cboDriver.SelectedText = "";
                cboDriver.Text = "";

                ODBCDriver[] DriverList = ODBCManager.GetODBCDrivers();
                if (DriverList != null)
                {
                    foreach (ODBCDriver driver in DriverList)
                    {
                        cboDriver.Items.Add(driver.GetODBCDriverName().ToString());

                    }
                    cboDriver.SelectedText = cboDriver.Items[0].ToString();
                }
            
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

}

        private void btnReadTest_Click(object sender, EventArgs e)
        {
            try
            {
                ODBCData c_ODBCData = new ODBCData();
                string DSN_Name = "";
                string Server = "";
                if (lblServer.Text == "DSN")
                    DSN_Name = cboDSNList.Text;
                if (lblServer.Text == "Server")
                    Server = cboDSNList.Text;

                string TestResult = c_ODBCData.ODBC_ReadTest(DSN_Name, cboDriver.Text, Server, cboDatabase.Text, txtUser.Text, txtPassword.Text, txtoptions.Text,txtReadQuery.Text);
                ListMessageLog.Items.Add(TestResult);


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btnWriteTest_Click(object sender, EventArgs e)
        {
            try
            {
                ODBCData c_ODBCData = new ODBCData();
                string DSN_Name = "";
                string Server = "";
                if (lblServer.Text == "DSN")
                    DSN_Name = cboDSNList.Text;
                if (lblServer.Text == "Server")
                    Server = cboDSNList.Text;

                string TestResult = c_ODBCData.ODBC_WriteTest(DSN_Name, cboDriver.Text, Server, cboDatabase.Text, txtUser.Text, txtPassword.Text, txtoptions.Text,txtWriteQuery.Text);
                ListMessageLog.Items.Add(TestResult);


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
    }
}
