﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Odbc;
using System.Data;

namespace ODBCMngr
{
    public class ODBCData
    {
        public string ODBC_ConnectionTest(string DSN_Name,string Driver_Name, string Server_Name, string Database, string UserName, string Password,string strOptions)
        {
            string Result = "";
            try
            {
                OdbcConnection conn = new OdbcConnection(ODBC_Connectionstring_Get(DSN_Name, Driver_Name, Server_Name, Database, UserName, Password, strOptions));
                conn.Open();
                if (conn.State == System.Data.ConnectionState.Open)
                    Result = "Connection successful!";
                conn.Close();
            }
            catch(Exception ex)
            {
                Result += "ERROR: " + ex.Message;
            }
            return Result;
        }

        public string ODBC_ReadTest(string DSN_Name, string Driver_Name, string Server_Name, string Database, string UserName, string Password, string strOptions, string strQuery)
        {
            string Result = "";
            try
            {
                OdbcConnection conn = new OdbcConnection(ODBC_Connectionstring_Get(DSN_Name, Driver_Name, Server_Name, Database, UserName, Password, strOptions));
                conn.Open();
                if (conn.State == System.Data.ConnectionState.Open)
                    Result = "Connection successful. ";

                DataTable dtTest = new DataTable();
                OdbcDataAdapter daTest = new OdbcDataAdapter(strQuery, conn);
                daTest.Fill(dtTest);

                if (dtTest != null && dtTest.Rows.Count > 0)
                    Result += "Successfully returned " + dtTest.Rows.Count.ToString() + " row(s)";
                else
                    Result += "No rows returned";



                conn.Close();
            }
            catch (Exception ex)
            {
                Result += "ERROR: " + ex.Message;
            }
            return Result;
        }

        public string ODBC_WriteTest(string DSN_Name, string Driver_Name, string Server_Name, string Database, string UserName, string Password, string strOptions, string strQuery)
        {
            string Result = "";
            try
            {
                OdbcConnection conn = new OdbcConnection(ODBC_Connectionstring_Get(DSN_Name, Driver_Name, Server_Name, Database, UserName, Password, strOptions));
                conn.Open();
                if (conn.State == System.Data.ConnectionState.Open)
                    Result = "Connection successful. ";
                OdbcCommand cmdTest = new OdbcCommand(strQuery, conn);
                int iCommandResult = cmdTest.ExecuteNonQuery();
                if (iCommandResult == 1)
                    Result += "The update command completed successfully";
                else
                    Result += "The update ran but returned a negative result";

                conn.Close();
            }
            catch (Exception ex)
            {
                Result += "ERROR: " + ex.Message;
            }
            return Result;
        }

        public string ODBC_Connectionstring_Get(string DSN_Name, string Driver_Name, string Server_Name, string Database,string UserName, string Password,string Options)
        {
            string ConnectionString = "";
            try
            {
                if (UserName == null) UserName = "";
                if (Password == null) Password = "";
                if(DSN_Name != null && DSN_Name != "")
                    ConnectionString = "DSN=" + DSN_Name + "; Uid=" + UserName + "; Pwd=" + Password + "; ";
                if (Driver_Name != null && Driver_Name != "")
                {
                    ConnectionString = "DRIVER ={" + Driver_Name + "};";
                    if (Server_Name != null && Server_Name != "") ConnectionString +=  "DSN = " + Server_Name + ";";
                    if (Database != null && Database != "") ConnectionString += "DATABASER = " + Database + ";";
                    ConnectionString += "Uid = " + UserName + "; Pwd = " + Password + "; ";

                }
                if(Options.Length > 3)
                {
                    ConnectionString += Options + ";";
                }
            }
            catch
            {

            }
            return ConnectionString;
        }


    }
}
